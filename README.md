##This script does the following:


1. generates 1000 instances of CanSignal with randomly generated properties
2. traverses each pair of signals ( **CanSignal** ) and checks whether they fit each other ( **verify(signal_a,signal_b)** )
3. if values of both signals are identical then this situation is assumed to be a crash ( "...caused a crash" message appears )
4. each treaversal step is printed to console.