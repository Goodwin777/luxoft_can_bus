import random
import string
import time


def howlong(f):
    def tmp(*args, **kwargs):
        t = time.time()
        res = f(*args, **kwargs)
        print "time: %f" % (time.time() - t)
        return res

    return tmp


class CanSignal:

    def __init__(self, name, current_value, value_range):
        self.__name = name
        self.__current_value = current_value
        self.__value_range = value_range

    def get_name(self):
        return self.__name

    def set_name(self, value):
        self.__name = value

    def get_value_range(self):
        return self.__value_range

    def set_value_range(self, value):
        self.__value_range = value

    def get_current_value(self):
        return self.__current_value

    def set_current_value(self, value):
        self.__current_value = value

    name = property(get_name, set_name)
    current_value = property(get_current_value, set_current_value)
    value_range = property(get_value_range, set_value_range)

    def __str__(self):
        return "{ name: " + self.get_name() + ", current_value: " + str(self.get_current_value()) + ", value_range: " + str(self.get_value_range()) + "}"


class Task:

    def generate_bool(self):
        return random.choice([False, True])

    def generate_integer(self):
        return random.randint(-1000, 1000)

    def generate_string(self):
        return "".join(random.SystemRandom().choice(
            string.ascii_uppercase + string.digits) for _ in range(5))

    def generate_signal(self, number=None):

        name = "signal_" + str(number)

        range_number = 10

        random_bools = []
        random_ints = []
        random_strings = []

        for i in xrange(0, range_number):

            random_bool = self.generate_bool()
            if random_bool not in random_bools:
                random_bools.append(random_bool)

            random_int = self.generate_integer()
            if random_int not in random_ints:
                random_ints.append(random_int)

            random_string = self.generate_string()
            if random_string not in random_strings:
                random_strings.append(random_string)

        value_range = random.choice(
            [random_bools, random_ints, random_strings])

        current_value = value_range[0]

        return CanSignal(name, current_value, value_range)

    def generate_signal_bus(self, number_of_signals):
        signal_bus = []
        for i in xrange(0, number_of_signals):
            signal_bus.append(self.generate_signal(i + 1))

        return signal_bus

    def verify(self, signal_a, signal_b):
        if signal_a.current_value == signal_b.current_value:
            print str(signal_a) + " and " + str(signal_b) + " caused a crash!!!"
        else:
            print str(signal_a) + " passed " + str(signal_b)

    @howlong
    def perform_task(self, number_of_signals):

        can_bus = self.generate_signal_bus(number_of_signals)
        n = len(can_bus)
        for i in xrange(0, n):
            for j in xrange(0, n):
                if i != j:
                    self.verify(can_bus[i], can_bus[j])

Task().perform_task(10)